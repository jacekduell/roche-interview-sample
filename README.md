Sample project for the interview purpose
-----------------

This is a demo application for managing products and orders via REST.
The default port 8080. 

The application uses a postgres database and H2 in memory for tests.

All the following commands assume the user is in the base directory of the project.

#### To build the app
To build the application type `mvnw clean package`

#### To run unit tests
To run unit tests type `mvnw clean test`

#### To run integration tests
To run all test suite also potentially long running integration tests type `mvnw clean verify -P failsave`

#### To run application
In order to use the app install a postgres database locally and create database roche_sample with user: postgres and password: password.
Then invoke `mvnw spring-boot:run`
Not to be obligated to install postgres locally start it in container before runnig the application with the following command `docker-compose -f src/main/docker/docker-compose.yml up -d db`

#### To look at the REST API documentation
After you run the application you will find documentation of the API at http://localhost:8080/swagger-ui.html . It was prepared with the help of swagger REST API documentation tool.

#### To run everything in containers with a single command (Postgres DB with app)
`docker-compose -f src/main/docker/docker-compose.yml up`
After invoking this commands the REST API calls can be invoked by the use of a rest client (eq. postman) similarly as setup locally. Base URL is localhost:8080 as well. Look at the API documentation to check it in detail. 

