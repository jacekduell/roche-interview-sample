package com.roche.interview.api;

import com.roche.interview.api.dto.PlaceItemDto;
import com.roche.interview.api.dto.PlaceOrderDto;
import com.roche.interview.api.dto.ProductDto;
import com.roche.interview.service.OrderService;
import com.roche.interview.service.ProductService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import javax.transaction.Transactional;

import static com.roche.interview.api.ProductControllerIntegrationTest.toJson;
import static java.time.LocalDateTime.now;
import static java.util.Collections.singletonList;
import static org.hamcrest.Matchers.is;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.MOCK;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = MOCK)
@AutoConfigureMockMvc
@AutoConfigureRestDocs
@Transactional
class OrderControllerIntegrateionTest {
    public static final String SKU_1 = "sku 1";
    public static final String PRODUCT_1 = "product 1";
    public static final String JACEK_DUELL_EMAIL = "jacek.duell@roche.com";

    @Autowired
    MockMvc mockMvc;

    @Autowired
    ProductService productService;
    @Autowired
    OrderService orderService;

    private ProductDto productDto;
    private PlaceOrderDto placeOrderDto;

    @BeforeEach
    void setUp() {
        //given
        productDto = ProductDto.builder()
                .sku(SKU_1)
                .name(PRODUCT_1)
                .price("1.5")
                .build();
        placeOrderDto = PlaceOrderDto.builder()
                .buyerEmail(JACEK_DUELL_EMAIL)
                .build();
    }

    @Test
    void searchOrders() throws Exception {
        //given
        preparePlaceOrderDto();
        orderService.placeOrder(placeOrderDto);

        //when
        mockMvc.perform(get("/api/v1/orders?from={from}&to={to}", now().minusHours(1), now()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.length()", is(1)))
                .andExpect(jsonPath("$[0].buyerEmail", is(JACEK_DUELL_EMAIL)))
                .andExpect(jsonPath("$[0].totalPrice", is("3.0")));
    }

    private void preparePlaceOrderDto() {
        ProductDto createdProduct = productService.create(productDto);
        Long id = createdProduct.getId();
        PlaceItemDto placeItemDto = PlaceItemDto.builder().productId(id).quantity(2).build();
        placeOrderDto.setPlaceItemDtos(singletonList(placeItemDto));
    }

    @Test
    void placeOrder() throws Exception {

        //given
        preparePlaceOrderDto();

        //when
        mockMvc.perform(post("/api/v1/orders")
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(placeOrderDto)))
                .andExpect(status().isOk());
    }

}