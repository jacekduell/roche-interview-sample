package com.roche.interview.api;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.roche.interview.api.dto.ProductDto;
import com.roche.interview.data.entity.Product;
import com.roche.interview.data.repository.ProductRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import javax.transaction.Transactional;
import java.math.BigDecimal;

import static org.hamcrest.Matchers.is;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.MOCK;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = MOCK)
@AutoConfigureMockMvc
@Transactional
class ProductControllerIntegrationTest {
    public static final String SKU_1 = "sku 1";
    public static final String PRODUCT_1 = "product 1";
    @Autowired
    MockMvc mockMvc;
    @Autowired
    ProductRepository productRepository;

    private Product product;
    private ProductDto productDto;

    @BeforeEach
    void setUp() {
        //given
        product = Product.builder()
                .sku(SKU_1)
                .name(PRODUCT_1)
                .price(BigDecimal.ONE)
                .build();
        productDto = ProductDto.builder()
                .sku(SKU_1)
                .name(PRODUCT_1)
                .price("1")
                .build();
    }

    @Test
    public void shouldGetAllProducts() throws Exception {
        //given
        productRepository.save(product);

        //when //then
        mockMvc.perform(get("/api/v1/products"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.length()", is(1)))
                .andExpect(jsonPath("$[0].sku", is(SKU_1)))
                .andExpect(jsonPath("$[0].name", is(PRODUCT_1)))
                .andExpect(jsonPath("$[0].price", is("1")));
    }

    @Test
    public void shouldCreateProduct() throws Exception {

        //when //then
        mockMvc.perform(post("/api/v1/products")
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(productDto)))
                .andExpect(status().isOk());
    }

    @Test
    public void shouldUpdateProduct() throws Exception {
        //given
        Product existingProduct = productRepository.save(product);

        //when //then
        mockMvc.perform(put("/api/v1/products/{id}", existingProduct.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(productDto)))
                .andExpect(status().isOk());
    }

    @Test
    public void shouldDeleteProduct() throws Exception {
        //given
        Product existingProduct = productRepository.save(product);

        //when //then
        mockMvc.perform(delete("/api/v1/products/{id}", existingProduct.getId()))
                .andExpect(status().isOk());
    }

    public static String toJson(Object o) throws JsonProcessingException {
        return new ObjectMapper().writeValueAsString(o);
    }
}