package com.roche.interview.api;

import com.roche.interview.api.dto.ProductDto;
import com.roche.interview.data.entity.Product;
import com.roche.interview.service.ProductService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;

import static com.roche.interview.api.ProductControllerIntegrationTest.toJson;
import static java.util.Collections.singletonList;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(ProductController.class)
class ProductControllerTest {
    public static final String SKU_1 = "sku 1";
    public static final String PRODUCT_1 = "product 1";

    @Autowired
    private MockMvc mvc;

    @MockBean
    private ProductService service;

    private Product product;
    private ProductDto productDto;

    @BeforeEach
    void setUp() {
        //given
        product = Product.builder()
                .id(1L)
                .sku(SKU_1)
                .name(PRODUCT_1)
                .price(BigDecimal.ONE)
                .build();
        productDto = ProductDto.builder()
                .sku(SKU_1)
                .name(PRODUCT_1)
                .price("1")
                .build();
    }
    @Test
    void shouldGetAll() throws Exception {

        //given
        given(service.findAll()).willReturn(singletonList(productDto));

        //when //then
        mvc.perform(get("/api/v1/products"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].sku", is(SKU_1)))
                .andExpect(jsonPath("$[0].name", is(PRODUCT_1)))
                .andExpect(jsonPath("$[0].price", is("1")));
    }

    @Test
    void shouldCreate() throws Exception {

        //when //then
        mvc.perform(post("/api/v1/products")
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(productDto)))
                .andExpect(status().isOk());
    }

    @Test
    void shouldUpdate() throws Exception {

        //when //then
        mvc.perform(put("/api/v1/products/{id}", 1L)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(productDto)))
                .andExpect(status().isOk());
    }

    @Test
    void shouldDelete() throws Exception {

        //when //then
        mvc.perform(delete("/api/v1/products/{id}", product.getId()))
                .andExpect(status().isOk());
    }
}