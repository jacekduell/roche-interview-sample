package com.roche.interview.service;

import com.roche.interview.api.dto.ProductDto;
import com.roche.interview.data.entity.Product;
import com.roche.interview.data.repository.ProductRepository;
import com.roche.interview.service.mapper.ProductMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

class ProductServiceTest {
    public static final String SKU_1 = "sku 1";
    public static final String PRODUCT_1 = "product 1";


    private ProductService productService;
    private ProductRepository productRepository;
    private ProductMapper productMapper;

    private Product product;
    private Product productWithId;
    private Product productUpdate;
    private ProductDto productDto;

    @BeforeEach
    void setUp() {
        //given
        productRepository = mock(ProductRepository.class);
        productMapper = Mappers.getMapper(ProductMapper.class);
        productService = new ProductService(productRepository, productMapper);
        product = Product.builder()
                .sku(SKU_1)
                .name(PRODUCT_1)
                .price(BigDecimal.ONE)
                .build();
        productWithId = Product.builder()
                .id(1L)
                .sku(SKU_1)
                .name(PRODUCT_1)
                .price(BigDecimal.ONE)
                .build();
        productUpdate = Product.builder()
                .id(1L)
                .sku("sku 1-updated")
                .name("product 1-updated")
                .price(BigDecimal.TEN)
                .build();
        productDto = ProductDto.builder()
                .sku(SKU_1)
                .name(PRODUCT_1)
                .price("1")
                .build();
    }


    @Test
    void shouldSaveProduct() {

        //given
        given(productRepository.save(product)).willReturn(productWithId);

        //when
        ProductDto createdProduct = productService.create(productDto);

        //then
        assertNotNull(createdProduct);
        assertEquals(createdProduct.getId(), 1L);
    }

    @Test
    void shouldFindAllProducts() {
        //given
        given(productRepository.findAll()).willReturn(singletonList(product));

        //when
        List<ProductDto> products = productService.findAll();

        //then
        assertThat(products).hasSize(1);
    }


    @Test
    void shouldUpdateProduct() {
        //given
        Long id = productWithId.getId();
        given(productRepository.findById(id)).willReturn(Optional.of(productWithId));
        given(productRepository.save(productUpdate)).willReturn(productWithId);

        //when
        productService.update(id, productDto);
    }

    @Test
    void shouldSoftDeleteProduct() {
        //given
        Long id = productWithId.getId();
        given(productRepository.findById(id)).willReturn(Optional.of(productWithId));

        //when
        productService.deleteById(id);

    }
}