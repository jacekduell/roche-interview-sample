package com.roche.interview.data.repository;

import com.roche.interview.data.entity.Product;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

@DataJpaTest
class ProductRepositoryIntegrationTest {

    public static final String SKU_1 = "sku1";
    public static final String PRODUCT_1 = "product 1";
    public static final String SKU_1_UPDATED = "sku 1-updated";
    public static final String PRODUCT_1_UPDATED = "product 1-updated";

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private ProductRepository productRepository;

    private Product product;
    private Product productUpdate;

    @BeforeEach
    void setUp() {
        // given
        product = Product.builder()
                .sku(SKU_1)
                .name(PRODUCT_1)
                .price(BigDecimal.ONE)
                .build();
        productUpdate = Product.builder()
                .sku(SKU_1_UPDATED)
                .name(PRODUCT_1_UPDATED)
                .price(BigDecimal.TEN)
                .build();
    }


    @Test
    void shouldSaveProduct() {
        // when
        productRepository.save(product);

        // then
        Product savedProduct = entityManager.find(Product.class, product.getId());
        assertNotNull(savedProduct);
        assertThat(savedProduct.getSku()).isEqualTo(SKU_1);
        assertThat(savedProduct.getName()).isEqualTo(PRODUCT_1);
        assertThat(savedProduct.getPrice()).isEqualTo(BigDecimal.ONE);
    }

    @Test
    void shouldFindAllProducts() {
        // given
        entityManager.persist(product);
        entityManager.persist(productUpdate);
        entityManager.flush();

        // when
        Iterable<Product> products = productRepository.findAll();

        // then
        assertThat(products).hasSize(2);
    }


    @Test
    void shouldUpdateProduct() {
        // given
        product = entityManager.persistAndFlush(product);
        entityManager.detach(product);
        productUpdate.setId(product.getId());

        // when
        productRepository.save(productUpdate);

        // then
        Product updatedProduct = entityManager.find(Product.class, product.getId());
        assertNotNull(updatedProduct);
        assertThat(updatedProduct.getSku()).isEqualTo(SKU_1_UPDATED);
        assertThat(updatedProduct.getName()).isEqualTo(PRODUCT_1_UPDATED);
        assertThat(updatedProduct.getPrice()).isEqualTo(BigDecimal.TEN);
    }

    @Test
    void shouldSoftDeleteProduct() {
        // given
        entityManager.persist(product);
        entityManager.flush();
        entityManager.detach(product);

        // when
        productRepository.delete(product);

        // then
        Product deletedProduct = entityManager.find(Product.class, product.getId());
        assertNotNull(deletedProduct);
        assertTrue(deletedProduct.getDeleted());
    }

}