package com.roche.interview.data.repository;

import com.roche.interview.data.entity.Item;
import com.roche.interview.data.entity.ItemKey;
import com.roche.interview.data.entity.Order;
import com.roche.interview.data.entity.Product;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import java.math.BigDecimal;
import java.util.List;

import static java.time.LocalDateTime.now;
import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
class OrderRepositoryIntegrationTest {
    public static final String SKU_1 = "sku 1";
    public static final String PRODUCT_1 = "product 1";
    public static final String JACEK_DUELL_EMAIL = "jacek.duell@roche.com";
    @Autowired
    private OrderRepository orderRepository;
    @Autowired
    private TestEntityManager em;

    private Product product;

    @BeforeEach
    void setUp() {
        product = Product.builder()
                .sku(SKU_1)
                .name(PRODUCT_1)
                .price(BigDecimal.ONE)
                .build();
    }

    @Test
    void shouldRetrieveOrdersInPeriod() {

        //given
        Order order = placeOrder();

        //when
        List<Order> ordersFound = orderRepository.findByDatetimeRange(now().minusDays(1), now());

        //then
        assertThat(ordersFound).hasSize(1);
        assertThat(ordersFound.get(0).getId()).isEqualTo(order.getId());
    }

    private Order placeOrder() {
        Product createdProduct = em.persistAndFlush(product);
        Item item = Item.builder()
                .id(new ItemKey(null, createdProduct.getId()))
                .product(createdProduct)
                .quantity(2)
                .build();
        Order order = Order.builder()
                .buyerEmail(JACEK_DUELL_EMAIL)
                .items(singletonList(item))
                .build();
        item.setOrder(order);
        Order createdOrder = em.persistAndFlush(order);
        return createdOrder;
    }

}