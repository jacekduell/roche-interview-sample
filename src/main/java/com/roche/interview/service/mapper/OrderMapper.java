package com.roche.interview.service.mapper;

import com.roche.interview.api.dto.OrderDto;
import com.roche.interview.data.entity.Order;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.math.BigDecimal;
import java.util.List;

@Mapper(componentModel = "spring")
public interface OrderMapper {

    List<OrderDto> toDto(List<Order> orders);

    @Mapping(target = "totalPrice", expression = "java(calculateTotalPrice(order))")
    OrderDto toDto(Order order);

    default String calculateTotalPrice(Order order) {
        BigDecimal calculatedPrice = order.getItems()
                .stream()
                .map(it -> it.getProduct().getPrice().multiply(BigDecimal.valueOf(it.getQuantity())))
                .reduce(BigDecimal::add)
                .orElse(new BigDecimal("0.0"));
        return calculatedPrice.toString();
    }
}
