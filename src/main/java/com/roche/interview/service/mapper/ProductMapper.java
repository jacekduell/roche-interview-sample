package com.roche.interview.service.mapper;

import com.roche.interview.api.dto.ProductDto;
import com.roche.interview.data.entity.Product;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ProductMapper {
    List<ProductDto> toDto(Iterable<Product> products);

    ProductDto toDto(Product products);

    Product toEntity(ProductDto dto);
}
