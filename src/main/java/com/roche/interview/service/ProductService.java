package com.roche.interview.service;

import com.roche.interview.api.dto.ProductDto;
import com.roche.interview.data.entity.Product;
import com.roche.interview.data.repository.ProductRepository;
import com.roche.interview.service.mapper.ProductMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.math.BigDecimal;
import java.util.List;

@Service
@Slf4j
@AllArgsConstructor
public class ProductService {
    private final ProductRepository productRepository;
    private final ProductMapper productMapper;

    public List<ProductDto> findAll() {
        Iterable<Product> products = productRepository.findAll();
        return productMapper.toDto(products);
    }

    public ProductDto create(ProductDto productDto) {
        Product product = productMapper.toEntity(productDto);
        Product createdProduct = productRepository.save(product);
        log.info("Product created {}", createdProduct);
        return productMapper.toDto(createdProduct);
    }

    public void update(long id, ProductDto productDto) {
        Product productToUpdate = productRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Product with does not exist, id=" + id));
        productToUpdate.setName(productDto.getName());
        productToUpdate.setSku(productDto.getSku());
        productToUpdate.setPrice(new BigDecimal(productDto.getPrice()));
        Product updatedProduct = productRepository.save(productToUpdate);
        log.info("Product product {}", updatedProduct);
    }

    public void deleteById(Long id) {
        productRepository.findById(id).ifPresent(this::delete);
    }

    private void delete(Product product) {
        productRepository.delete(product);
        log.info("Product deleted {}", product);
    }
}
