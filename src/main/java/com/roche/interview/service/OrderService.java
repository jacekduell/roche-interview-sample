package com.roche.interview.service;

import com.roche.interview.api.dto.OrderDto;
import com.roche.interview.api.dto.PlaceItemDto;
import com.roche.interview.api.dto.PlaceOrderDto;
import com.roche.interview.data.entity.Item;
import com.roche.interview.data.entity.ItemKey;
import com.roche.interview.data.entity.Order;
import com.roche.interview.data.entity.Product;
import com.roche.interview.data.repository.OrderRepository;
import com.roche.interview.data.repository.ProductRepository;
import com.roche.interview.service.mapper.OrderMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.time.LocalDateTime;
import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
@Slf4j
@AllArgsConstructor
public class OrderService {
    private final ProductRepository productRepository;
    private final OrderRepository orderRepository;
    private final OrderMapper orderMapper;

    public void placeOrder(PlaceOrderDto orderRequest) {
        String buyerEmail = orderRequest.getBuyerEmail();
        List<PlaceItemDto> placeItemDtos = orderRequest.getPlaceItemDtos();
        List<Item> items = toItems(placeItemDtos);
        Order order = Order.builder()
                .buyerEmail(buyerEmail)
                .build();
        bindItemsToOrder(items, order);
        Order savedOrder = orderRepository.save(order);
        log.info("Placed order {}", savedOrder);
    }

    public List<OrderDto> searchOrders(LocalDateTime from, LocalDateTime to) {
        List<Order> orders = orderRepository.findByDatetimeRange(from, to);
        return orderMapper.toDto(orders);
    }


    private List<Item> toItems(List<PlaceItemDto> placeItemDtos) {
        return placeItemDtos
                .stream()
                .map(dto -> createLineItem(dto.getProductId(), dto.getQuantity()))
                .collect(toList());
    }

    private void bindItemsToOrder(List<Item> items, Order order) {
        items.forEach(item -> item.setOrder(order));
        order.setItems(items);
    }

    private Item createLineItem(Long productId, Integer quantity) {
        Product product = productRepository.findById(productId).orElseThrow(() -> new EntityNotFoundException("Product not found, id=" + productId));
        ItemKey itemKey = new ItemKey(null, product.getId());
        return Item.builder()
                .id(itemKey)
                .product(product)
                .quantity(quantity)
                .build();
    }
}
