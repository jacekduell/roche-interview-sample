package com.roche.interview.api;

import com.roche.interview.api.dto.OrderDto;
import com.roche.interview.api.dto.PlaceOrderDto;
import com.roche.interview.service.OrderService;
import lombok.AllArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("/api/v1/orders")
@AllArgsConstructor
public class OrderController {
    private final OrderService orderService;

    @GetMapping
    public List<OrderDto> getOrders(@RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)  LocalDateTime from,
                                    @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime to) {
        return orderService.searchOrders(from, to);
    }

    @PostMapping
    public ResponseEntity<Void> placeOrder(@RequestBody @Valid PlaceOrderDto orderRequest) {
        orderService.placeOrder(orderRequest);
        return ResponseEntity.ok().build();
    }
}
