package com.roche.interview.api.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PlaceItemDto {
    @NotNull
    private Integer quantity;
    @NotNull
    private Long productId;
}
