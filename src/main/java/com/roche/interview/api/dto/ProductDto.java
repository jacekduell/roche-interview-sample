package com.roche.interview.api.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProductDto {
    private Long id;

    @NotBlank
    private String sku;
    @NotBlank
    private String name;
    @NotBlank
    private String price;

    private LocalDateTime createdDatetime;
    private Boolean deleted;

}