package com.roche.interview.api.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PlaceOrderDto {
    @NotNull
    @Email
    private String buyerEmail;
    @NotEmpty
    @Valid
    private List<PlaceItemDto> placeItemDtos;
}
