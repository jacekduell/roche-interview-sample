package com.roche.interview.data.entity;


import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import static lombok.AccessLevel.PROTECTED;

@Entity
@Table(name = "item")
@Data
@Builder
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@ToString(of = {"product", "quantity"})
@AllArgsConstructor(access = PROTECTED)
@NoArgsConstructor
public class Item {

    @EmbeddedId
    @EqualsAndHashCode.Include
    private ItemKey id;

    @ManyToOne
    @MapsId("orderId")
    private Order order;

    @ManyToOne
    @MapsId("productId")
    private Product product;

    @Min(1)
    @NotNull
    @Column(nullable = false)
    private Integer quantity;

}
