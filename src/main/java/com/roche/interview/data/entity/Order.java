package com.roche.interview.data.entity;


import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

import static javax.persistence.CascadeType.ALL;
import static lombok.AccessLevel.PROTECTED;

@Entity
@Table(name = "customer_order")
@Data
@Builder
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@AllArgsConstructor(access = PROTECTED)
@NoArgsConstructor
public class Order {
    @Id
    @GeneratedValue
    @EqualsAndHashCode.Include
    private Long id;

    @Column(nullable = false)
    private UUID orderId;

    @NotEmpty
    @OneToMany(mappedBy = "order", cascade = ALL)
    private List<Item> items;

    @Email
    @NotNull
    @Column(nullable = false)
    private String buyerEmail;

    @Column(nullable = false)
    private LocalDateTime createdDatetime;

    @PrePersist
    public void onPrePersist() {
        setCreatedDatetime(LocalDateTime.now());
        setOrderId(UUID.randomUUID());
    }

}
