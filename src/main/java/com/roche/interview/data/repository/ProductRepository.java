package com.roche.interview.data.repository;

import com.roche.interview.data.entity.Product;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends CrudRepository<Product, Long> {

    @Modifying
    @Query("UPDATE Product p SET p.deleted = TRUE WHERE p = ?1")
    void delete(Product product);
}
