package com.roche.interview.data.repository;

import com.roche.interview.data.entity.Order;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface OrderRepository extends CrudRepository<Order, Long> {

    @Query("FROM Order o WHERE o.createdDatetime >= ?1 AND o.createdDatetime <= ?2")
    List<Order> findByDatetimeRange(LocalDateTime from, LocalDateTime to);
}
